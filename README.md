.

**Framework for Open-source BLAS  Benchmarking**
========================================
**University of Manchester (UK)**

** The Numerical Algorithms Group Ltd (UK)**

About
====

The purpose of this code is a fair comparison of different implementations
of the Basic Linear Algebra Subroutine (BLAS) libraries. It is based on the tests of the
PLASMA library, but customized. 

At the moment, no automatic code generation is enabled, and only 
double precision (the `d' routines)  and single precision (the 's' routines) are generated.
The following BLAS routines are supported:

* ?gemm (Computes a matrix-matrix product with general matrices)
* ?symm (Computes a matrix-matrix product where one input matrix is symmetric)
* ?syrk (Performs a symmetric rank-k update)
* ?syr2k (Performs a symmetric rank-2k update)
* ?trmm (Computes a matrix-matrix product where one input matrix is triangular)
* ?trsm (Solves a triangular matrix equation)

and compares these implementations in  

* [ATLAS](http://math-atlas.sourceforge.net/) 
* [BLIS](https://github.com/flame/blis)
* [OpenBLAS](https://www.openblas.net/)
* [The Netlib reference Implementation](http://www.netlib.org/blas/)

vendor libraries:

* [MKL](https://software.intel.com/en-us/mkl) for Intel architectures
* [ARMpl](https://developer.arm.com/products/software-development-tools/hpc/arm-performance-libraries/getting-started) for ARM architectures

and 

* [The NAG BLAS from libnagnag](https://www.nag.co.uk/content/downloads-nag-c-library-versions)

Additional requirement
---------------------
In addition to the libraries to compare, the [Netlib lapacke](http://www.netlib.org/lapack/) library 
is also required for matrice generation and norm functions. 

Usage
-----

### Compiling

1. Create a file

       src/make.inc

  with settings specific to a machine. 


  The important setting in `make.inc` are correct *paths to the four libraries*
  we are comparing.

2. All binaries should be created in the `bin` folder by

       ./compile_all_tests.sh

### Running the tests

1. Edit file

       ./run_tests.sh

  and use meaningful values for your machine. One should set the minimum and
  maximum matrix size and its stepping for the performance test.
  In addition, minimum, maximum, and stepping of the number of cores need to be
  set for the scalability test. Performance test will be run on maximum number 
  of cores. Scalability test will be performed on maximum matrix size.



