# setting particular for a machine

CC=gcc
CFLAGS=-fopenmp $(FPIC) -O3 -g  -std=gnu99 -Wall -pedantic -Wshadow -Wno-unused
# library to link with in case of LAPACK
LIBFORTRAN = -lgfortran

LAPACK_DIR = /ccsopen/home/mzounon/Software/lapack-3.8.0
ATLAS_DIR = /ccsopen/home/mzounon/Software/atlas
BLIS_DIR = /ccsopen/home/mzounon/Software/blis
OPENBLAS_DIR = /ccsopen/home/mzounon/Software/openblas
NAG_DIR = /ccsopen/home/mzounon/Software/nag/BUILD_AMD64_GFORTRAN_64_int64/nag/climp
# link line for threaded ARMpl
ARM_DIR =  /autofs/nccs-svm1_wombat_sw/ARM_Compiler/19.0/opt/arm/armpl-19.0.0_Generic-AArch64_RHEL-7_gcc_8.2.0_aarch64-linux


