#!/bin/bash
LIBS='atlas blis netlib openblas arm nag'
COMPILERS='gnu'
# create directory for binaries if not existing
BIN_DIR='./bin'
mkdir -p ${BIN_DIR}
for lib in ${LIBS}; do
    # compile with the correct paths
    make -B BLASLIB=${lib}  -j;
done
