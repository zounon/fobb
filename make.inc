# setting particular for a machine

CC=gcc
CFLAGS=-fopenmp $(FPIC) -O3 -std=gnu99 -Wall -pedantic -Wshadow -Wno-unused
# library to link with in case of LAPACK
LIBFORTRAN = -lgfortran

LAPACK_DIR = /home/zounonmawussi/Software/lapack-3.8.0
ATLAS_DIR = /home/zounonmawussi/Software/atlas
BLIS_DIR = /home/zounonmawussi/Software/blis
OPENBLAS_DIR = /home/zounonmawussi/Software/openblas
NAG_DIR = /home/zounonmawussi/Software/nag/BUILD_EM64T_MKL_64_int64/nag/climp
#NAG_DIR = /home/zounonmawussi/Software/nag/BUILD_linux_gfortran_64_int64/climp
# link line for threaded MKL
MKL_DIR = $(MKLROOT)


