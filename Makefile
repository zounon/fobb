include make.inc
# you should not need to edit it below here ###################################
#BLASLIB = atlas
# BLASLIB = blis
 BLASLIB = mkl
# BLASLIB = openblas
# BLASLIB = netlib
# BLASLIB = nag
##############################

$(info )
$(info ***************************************)
$(info Compiling with $(BLASLIB))
$(info ***************************************)
$(info )

ifeq ($(strip $(BLASLIB)),atlas)
    INC =  -I$(ATLAS_DIR)/include
    LIBS = -L$(ATLAS_DIR)/lib  -lptcblas -lptf77blas  -latlas
    #LIBS = -L$(ATLAS_DIR)/lib  -lcblas -lf77blas  -latlas # sequential
    CFLAGS += -DATLAS
endif


ifeq ($(strip $(BLASLIB)),blis)
    INC =  -I$(BLIS_DIR)/include/blis
    #LIBS = -L$(BLIS_DIR)/lib  -lblis
    LIBS = $(BLIS_DIR)/lib/libblis.a 
    CFLAGS += -DBLIS
endif


ifeq ($(strip $(BLASLIB)),openblas)
    INC =  -I$(OPENBLAS_DIR)/include
    LIBS = $(OPENBLAS_DIR)/lib/libopenblas.a
    CFLAGS += -DOPENBLAS
endif

ifeq ($(strip $(BLASLIB)),nag)
    INC =  -I$(NAG_DIR)/include -I$(LAPACK_DIR)/CBLAS/include
    LIBS = -L$(NAG_DIR)/lib -lnagc_nag 
    CFLAGS += -DNAG
endif

ifeq ($(strip $(BLASLIB)),netlib)
   # add INC and LIBS  below common to all
   INC =  -I$(LAPACK_DIR)/CBLAS/include -I$(LAPACK_DIR)/LAPACKE/include
   LIBS = -L$(LAPACK_DIR) -llapacke -llapack -lcblas -lrefblas -lgfortran
    CFLAGS += -DNETLIB
endif


ifeq ($(strip $(BLASLIB)),mkl)
    INC     = -I$(MKL_DIR)/include
    LIBS    = -L$(MKL_DIR)/lib/intel64 -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core \
                $(LIBFORTRAN) -ldl -lgomp
    CFLAGS += -DMKL
endif


 # need flops.h in all cases
INC  += -Iinclude
# for correctness checking (Netlib blas and lapack for all)
ifneq ($(strip $(BLASLIB)),nag)
	INC  += -I$(NAG_DIR)/include
	LIBS += -L$(NAG_DIR)/lib -lnagc_nag
endif

ifeq (, $(filter $(BLASLIB), netlib mkl))
	INC  += -I$(LAPACK_DIR)/CBLAS/include -I$(LAPACK_DIR)/LAPACKE/include
	LIBS += -L$(LAPACK_DIR) -llapacke $(LAPACK_DIR)/liblapack.a -lcblas -lrefblas -lgfortran
endif

ifeq ($(strip $(BLASLIB)),mkl)
LIBS += -lgfortran
endif

LIBS +=  -ldl  -lm  -lgomp -lpthread  -lirc -lifcore -liomp5  
#LIBS +=   -ldl  -lm  -lgomp -lpthread

all: test

TEST_SRC_FILES =  test.c \
     test_dgemm.c  test_dsymm.c  \
     test_dtrmm.c  test_dtrsm.c


TEST_SRC = $(addprefix src/, $(TEST_SRC_FILES))

TEST_OBJECT = $(TEST_SRC:.c=.o)

test: $(TEST_OBJECT)
	$(CC) $(CFLAGS) $(INC) $(LDFLAGS) $^ -o bin/test_$(BLASLIB) $(LIBS)

%.o: %.c
	$(CC) -c $(CFLAGS) $(INC) $< -o $@

clean:
	rm -f  *~ include/*~ src/*~  src/*.o 

.PHONY: clean
