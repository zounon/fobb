#include "test.h"
#include "flops.h"
#include <math.h>
#include <string.h>

//============================================
//    Include files for orrectness checking
//============================================
#if !defined(MKL)
    #include <cblas.h>
    #include <lapacke.h>
#endif
#include <nag.h>
#include <nagf16.h>
#include <nagf07.h>


#if defined(ATLAS) ||  defined(BLIS)
    // Intel mkl_cblas.h and Netlib do: typedef enum {...} CBLAS_ORDER;
    // OpenBLAS  cblas.h does: typedef enum CBLAS_ORDER {...} CBLAS_ORDER;
    // BLIS cblas.h and ATLAS do enum CBLAS_ORDER {...}
    // We use (CBLAS_ORDER), so add these typedefs for BLIS and ATLAS.
    #ifndef OPENBLAS_VERSION
        typedef enum CBLAS_ORDER CBLAS_ORDER;
        typedef enum CBLAS_TRANSPOSE CBLAS_TRANSPOSE;
        typedef enum CBLAS_UPLO CBLAS_UPLO;
        typedef enum CBLAS_DIAG CBLAS_DIAG;
        typedef enum CBLAS_SIDE CBLAS_SIDE;
   #endif
#elif defined(MKL)
    #include <mkl.h>
#endif

#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <omp.h>

/***************************************************************************//**
 *
 * @brief Tests DSYMM.
 *
 * @param[in]  param - array of parameters
 * @param[out] info  - string of column labels or column values; length InfoLen
 *
 * If param is NULL and info is NULL,     print usage and return.
 * If param is NULL and info is non-NULL, set info to column labels and return.
 * If param is non-NULL and info is non-NULL, set info to column values
 * and run test.
 ******************************************************************************/
void test_dsymm(param_value_t param[], char *info)
{
    //================================================================
    // Print usage info or return column labels or values.
    //================================================================
    if (param == NULL) {
        if (info == NULL) {
            // Print usage info.
            print_usage(PARAM_SIDE);
            print_usage(PARAM_UPLO);
            print_usage(PARAM_M);
            print_usage(PARAM_N);
        }
        else {
            // Return column labels.
            snprintf(info, InfoLen,
                     "%*s %*s %*s %*s",
                     InfoSpacing, "Side",
                     InfoSpacing, "Uplo",
                     InfoSpacing, "M",
                     InfoSpacing, "N");
        }
        return;
    }
    // Return column values.
    snprintf(info, InfoLen,
             "%*c %*c %*d %*d",
             InfoSpacing, param[PARAM_SIDE].c,
             InfoSpacing, param[PARAM_UPLO].c,
             InfoSpacing, param[PARAM_M].i,
             InfoSpacing, param[PARAM_N].i);

    //================================================================
    // Set parameters.
    //================================================================
    char side_c = param[PARAM_SIDE].c;
    char uplo_c = param[PARAM_UPLO].c;

    int m = param[PARAM_M].i;
    int n = param[PARAM_N].i;

    int Am, An;
    int Bm, Bn;
    int Cm, Cn;
    CBLAS_SIDE side;
    CBLAS_UPLO uplo;

    if (uplo_c == 'u') {
        uplo = CblasUpper;
    }
    else {
        uplo = CblasLower;
    }

    if (side_c == 'l') {
        side = CblasLeft;
        Am = m;
        An = m;
    }
    else {
        side = CblasRight;
        Am = n;
        An = n;
    }
    Bm = m;
    Bn = n;

    Cm = m;
    Cn = n;

    int lda = imax(1, Am);
    int ldb = imax(1, Bm);
    int ldc = imax(1, Cm);

    int test = param[PARAM_TEST].c == 'y';
    double eps = LAPACKE_dlamch('E');

    double alpha = 1.0;
    double beta  = 1.0;


    //================================================================
    // Allocate and initialize arrays.
    //================================================================
    double *A =
        (double*)malloc((size_t)lda*An*sizeof(double));
    assert(A != NULL);

    double *B =
        (double*)malloc((size_t)ldb*Bn*sizeof(double));
    assert(B != NULL);

    double *C =
        (double*)malloc((size_t)ldc*Cn*sizeof(double));
    assert(C != NULL);

    int seed[] = {0, 0, 0, 1};
    lapack_int retval;
    retval = LAPACKE_dlarnv(1, seed, (size_t)lda*An, A);
    assert(retval == 0);

    retval = LAPACKE_dlarnv(1, seed, (size_t)ldb*Bn, B);
    assert(retval == 0);

    retval = LAPACKE_dlarnv(1, seed, (size_t)ldc*Cn, C);
    assert(retval == 0);

    double *Cref = NULL;
    if (test) {
        Cref = (double*)malloc(
            (size_t)ldc*Cn*sizeof(double));
        assert(Cref != NULL);

        memcpy(Cref, C, (size_t)ldc*Cn*sizeof(double));
    }
    //================================================================
    // Run and time.
    //================================================================
    double start = omp_get_wtime();
    NagError fail;
    INIT_FAIL(fail);
    #if defined(NAG)
    nag_dsymm(
        CblasColMajor,
        side, uplo,
        m, n,
        alpha, A, lda,
        B, ldb,
        beta, C, ldc, &fail);
    #else
    cblas_dsymm(
        CblasColMajor,
        side, uplo,
        m, n,
        alpha, A, lda,
        B, ldb,
        beta, C, ldc);
    #endif
    double stop = omp_get_wtime();
    double time = stop-start;
    param[PARAM_TIME].d = time;
    param[PARAM_GFLOPS].d = flops_dsymm(side, m, n) / time / 1e9;

    //================================================================
    // Test results by comparing to a reference implementation.
    //================================================================
    if (test) {
        // see comments in test_dgemm.c
        char uplo_ = param[PARAM_UPLO].c;
        double work[1];
        double Anorm = LAPACKE_dlansy_work(
                           LAPACK_COL_MAJOR, 'F', uplo_, An, A, lda, work);
        double Bnorm = LAPACKE_dlange_work(
                           LAPACK_COL_MAJOR, 'F', Bm, Bn, B,    ldb, work);
        double Cnorm = LAPACKE_dlange_work(
                           LAPACK_COL_MAJOR, 'F', Cm, Cn, Cref, ldc, work);
        INIT_FAIL(fail);
        nag_dsymm(
            CblasColMajor,
            (CBLAS_SIDE) side, (CBLAS_UPLO) uplo,
            m, n,
            (alpha), A, lda,
                                B, ldb,
            (beta),  Cref, ldc, &fail);

        double zmone = -1.0;
        cblas_daxpy((size_t)ldc*Cn, (zmone), Cref, 1, C, 1);

        double error = LAPACKE_dlange_work(
                           LAPACK_COL_MAJOR, 'F', Cm, Cn, C,    ldc, work);
        double normalize = sqrt((double)An+2) * fabs(alpha) * Anorm * Bnorm
                         + 2 * fabs(beta) * Cnorm;
        if (normalize != 0)
            error /= normalize;

        param[PARAM_ERROR].d = error;
        param[PARAM_SUCCESS].i = error < 3*eps;
    }
    //================================================================
    // Free arrays.
    //================================================================
    free(A);
    free(B);
    free(C);
    if (test)
        free(Cref);
}
