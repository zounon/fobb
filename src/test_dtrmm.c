#include "test.h"
#include "flops.h"
#include <math.h>
#include <string.h>

//============================================
//    Include files for orrectness checking
//============================================
#if !defined(MKL)
    #include <cblas.h>
    #include <lapacke.h>
#endif
#include <nag.h>
#include <nagf16.h>
#include <nagf07.h>


#if defined(ATLAS) ||  defined(BLIS)
    // Intel mkl_cblas.h and Netlib do: typedef enum {...} CBLAS_ORDER;
    // OpenBLAS  cblas.h does: typedef enum CBLAS_ORDER {...} CBLAS_ORDER;
    // BLIS cblas.h and ATLAS do enum CBLAS_ORDER {...}
    // We use (CBLAS_ORDER), so add these typedefs for BLIS and ATLAS.
    #ifndef OPENBLAS_VERSION
        typedef enum CBLAS_ORDER CBLAS_ORDER;
        typedef enum CBLAS_TRANSPOSE CBLAS_TRANSPOSE;
        typedef enum CBLAS_UPLO CBLAS_UPLO;
        typedef enum CBLAS_DIAG CBLAS_DIAG;
        typedef enum CBLAS_SIDE CBLAS_SIDE;
   #endif
#elif defined(MKL)
    #include <mkl.h>
#endif

#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <omp.h>


/***************************************************************************//**
 *
 * @brief Tests DTRMM.
 *
 * @param[in]  param - array of parameters
 * @param[out] info  - string of column labels or column values; length InfoLen
 *
 * If param is NULL and info is NULL,     print usage and return.
 * If param is NULL and info is non-NULL, set info to column labels and return.
 * If param is non-NULL and info is non-NULL, set info to column values
 * and run test.
 ******************************************************************************/
void test_dtrmm(param_value_t param[], char *info)
{
    //================================================================
    // Print usage info or return column labels or values.
    //================================================================
    if (param == NULL) {
        if (info == NULL) {
            // Print usage info.
            print_usage(PARAM_SIDE);
            print_usage(PARAM_UPLO);
            print_usage(PARAM_TRANSA);
            print_usage(PARAM_DIAG);
            print_usage(PARAM_M);
            print_usage(PARAM_N);
        }
        else {
            // Return column labels.
            snprintf(info, InfoLen,
                     "%*s %*s %*s %*s %*s %*s",
                     InfoSpacing, "Side",
                     InfoSpacing, "Uplo",
                     InfoSpacing, "TransA",
                     InfoSpacing, "Diag",
                     InfoSpacing, "M",
                     InfoSpacing, "N");
        }
        return;
    }
    // Return column values.
    snprintf(info, InfoLen,
             "%*c %*c %*c %*c %*d %*d",
             InfoSpacing, param[PARAM_SIDE].c,
             InfoSpacing, param[PARAM_UPLO].c,
             InfoSpacing, param[PARAM_TRANSA].c,
             InfoSpacing, param[PARAM_DIAG].c,
             InfoSpacing, param[PARAM_M].i,
             InfoSpacing, param[PARAM_N].i);

    //================================================================
    // Set parameters.
    //================================================================
    char sidec = param[PARAM_SIDE].c;
    char uploc = param[PARAM_UPLO].c;
    char transac = param[PARAM_TRANSA].c;
    char diagc = param[PARAM_DIAG].c;

    CBLAS_TRANSPOSE transa;
    CBLAS_SIDE side;
    CBLAS_UPLO uplo;
    CBLAS_DIAG diag;

    int m = param[PARAM_M].i;
    int n = param[PARAM_N].i;

    int k;
    int lda;

    if (transac == 'n') {
        transa = CblasTrans;
    }
    else {
        transa = CblasNoTrans;
    }
    if (uploc == 'u' ) {
        uplo = CblasUpper;
    }
    else {
        uplo = CblasLower;
    } 
    if (diagc == 'u') {
        diag = CblasUnit;
    }
    else {
        diag = CblasNonUnit;
    }

    if (sidec == 'l') {
        side = CblasLeft;
        k    = m;
        lda  = imax(1, m);
    }
    else {
        side = CblasRight;
        k    = n;
        lda  = imax(1, n);
    }

    int    ldb  = imax(1, m);
    double alpha = 1.0;
    double eps  = LAPACKE_dlamch('E');
    int    test = param[PARAM_TEST].c == 'y';
    //================================================================
    // Allocate and initialize arrays.
    //================================================================
    double *A =
        (double *)malloc((size_t)lda*k*sizeof(double));
    assert(A != NULL);

    double *B =
        (double *)malloc((size_t)ldb*n*sizeof(double));
    assert(B != NULL);

    int seed[] = {0, 0, 0, 1};
    lapack_int retval;
    retval = LAPACKE_dlarnv(1, seed, (size_t)lda*k, A);
    assert(retval == 0);

    retval = LAPACKE_dlarnv(1, seed, (size_t)ldb*n, B);
    assert(retval == 0);

    double *Bref = NULL;
    if (test) {
        Bref = (double*)malloc(
            (size_t)ldb*n*sizeof(double));
        assert(Bref != NULL);

        memcpy(Bref, B, (size_t)ldb*n*sizeof(double));
    }

    //================================================================
    // Run and time.
    //================================================================
    NagError fail;
    INIT_FAIL(fail);
    double start = omp_get_wtime();
    #if defined(NAG)
    nag_dtrmm(
        CblasColMajor,
        side, uplo,
        transa, diag,
        m, n,
        alpha, A, lda,
        B, ldb, &fail);
    #else
    cblas_dtrmm(
        CblasColMajor,
        side, uplo,
        transa, diag,
        m, n,
        alpha, A, lda,
        B, ldb);
    #endif
    double  stop = omp_get_wtime();
    double  time = stop-start;

    param[PARAM_TIME].d = time;
    param[PARAM_GFLOPS].d = flops_dtrmm(side, m, n) / time / 1e9;

    //================================================================
    // Test results by comparing to a reference implementation.
    //================================================================
    if (test) {
        // see comments in test_dgemm.c
        double zmone = -1.0;
        double work[1];

        // LAPACKE_[ds]lantr_work has a bug (returns 0)
        // in MKL <= 11.3.3 (at least). Fixed in LAPACK 3.6.1.
        // For now, call LAPACK directly.
        // LAPACK_dlantr is a macro for correct name mangling (e.g.
        // adding _ at the end) of the Fortran symbol.
        // The macro is either defined in lapacke.h, or in the file
        // core_lapack_d.h for the use with MKL.
        char normc = 'F';
        // double Anorm = LAPACK_dlantr(&normc, &uploc, &diagc,
        //                                     &k, &k, A, &lda, work);
        double Anorm = LAPACKE_dlantr_work(
                          LAPACK_COL_MAJOR, 'F', uploc,
                          diagc, k, k, A, lda, work);

        double Bnorm = LAPACKE_dlange_work(
                           LAPACK_COL_MAJOR, 'F', m, n, Bref, ldb, work);
        INIT_FAIL(fail);
        nag_dtrmm(CblasColMajor, (CBLAS_SIDE)side, (CBLAS_UPLO)uplo,
                   (CBLAS_TRANSPOSE)transa, (CBLAS_DIAG)diag,
                  m, n, (alpha), A, lda, Bref, ldb, &fail);

        cblas_daxpy((size_t)ldb*n, (zmone), Bref, 1, B, 1);

        double error = LAPACKE_dlange_work(
                           LAPACK_COL_MAJOR, 'F', m, n, B,    ldb, work);
        double normalize = sqrt((double)k+2) * fabs(alpha) * Anorm * Bnorm;
        if (normalize != 0)
            error /= normalize;

        param[PARAM_ERROR].d = error;
        param[PARAM_SUCCESS].i = error < 3*eps;
    }

    //================================================================
    // Free arrays.
    //================================================================
    free(A);
    free(B);
    if (test)
        free(Bref);
}

