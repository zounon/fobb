#include "test.h"
#include "flops.h"
#include <math.h>
#include <string.h>

//============================================
//    Include files for orrectness checking
//============================================
#if !defined(MKL)
    #include <cblas.h>
    #include <lapacke.h>
#endif
#include <nag.h>
#include <nagf16.h>
#include <nagf07.h>


#if defined(ATLAS) ||  defined(BLIS)
    // Intel mkl_cblas.h and Netlib do: typedef enum {...} CBLAS_ORDER;
    // OpenBLAS  cblas.h does: typedef enum CBLAS_ORDER {...} CBLAS_ORDER;
    // BLIS cblas.h and ATLAS do enum CBLAS_ORDER {...}
    // We use (CBLAS_ORDER), so add these typedefs for BLIS and ATLAS.
    #ifndef OPENBLAS_VERSION
        typedef enum CBLAS_ORDER CBLAS_ORDER;
        typedef enum CBLAS_TRANSPOSE CBLAS_TRANSPOSE;
        typedef enum CBLAS_UPLO CBLAS_UPLO;
        typedef enum CBLAS_DIAG CBLAS_DIAG;
        typedef enum CBLAS_SIDE CBLAS_SIDE;
   #endif
#elif defined(MKL)
    #include <mkl.h>
#endif

#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <omp.h>

#define A(i_, j_) A[(i_) + (size_t)lda*(j_)]

/***************************************************************************//**
 *
 * @brief Tests DTRSM.
 *
 * @param[in]  param - array of parameters
 * @param[out] info  - string of column labels or column values; length InfoLen
 *
 * If param is NULL and info is NULL,     print usage and return.
 * If param is NULL and info is non-NULL, set info to column labels and return.
 * If param is non-NULL and info is non-NULL, set info to column values
 * and run test.
 ******************************************************************************/
void test_dtrsm(param_value_t param[], char *info)
{
    //================================================================
    // Print usage info or return column labels or values.
    //================================================================
    if (param == NULL) {
        if (info == NULL) {
            // Print usage info.
            print_usage(PARAM_SIDE);
            print_usage(PARAM_UPLO);
            print_usage(PARAM_TRANSA);
            print_usage(PARAM_DIAG);
            print_usage(PARAM_M);
            print_usage(PARAM_N);
        }
        else {
            // Return column labels.
            snprintf(info, InfoLen,
                     "%*s %*s %*s %*s %*s %*s",
                     InfoSpacing, "Side",
                     InfoSpacing, "Uplo",
                     InfoSpacing, "TransA",
                     InfoSpacing, "Diag",
                     InfoSpacing, "M",
                     InfoSpacing, "N");
        }
        return;
    }
    // Return column values.
    snprintf(info, InfoLen,
             "%*c %*c %*c %*c %*d %*d",
             InfoSpacing, param[PARAM_SIDE].c,
             InfoSpacing, param[PARAM_UPLO].c,
             InfoSpacing, param[PARAM_TRANSA].c,
             InfoSpacing, param[PARAM_DIAG].c,
             InfoSpacing, param[PARAM_M].i,
             InfoSpacing, param[PARAM_N].i);

    //================================================================
    // Set parameters.
    //================================================================
    char sidec = param[PARAM_SIDE].c;
    char uploc = param[PARAM_UPLO].c;
    char transac = param[PARAM_TRANSA].c;
    char diagc = param[PARAM_DIAG].c;

    CBLAS_TRANSPOSE transa;
    CBLAS_SIDE side;
    CBLAS_UPLO uplo;
    CBLAS_DIAG diag;

    int m = param[PARAM_M].i;
    int n = param[PARAM_N].i;
    int Am;

    if (transac == 'n') {
        transa = CblasTrans;
    }
    else {
        transa = CblasNoTrans;
    }
    if (uploc == 'u' ) {
        uplo = CblasUpper;
    }
    else {
        uplo = CblasLower;
    } 
    if (diagc == 'u') {
        diag = CblasUnit;
    }
    else {
        diag = CblasNonUnit;
    }
    if (sidec == 'l') {
        side = CblasLeft;
        Am = m;
    }
    else {
        side = CblasRight;
        Am = n;
    }

    int lda = imax(1, Am);
    int ldb = imax(1, m );

    int test = param[PARAM_TEST].c == 'y';
    double tol = param[PARAM_TOL].d * LAPACKE_dlamch('E');

    double alpha = 1.0;


    //================================================================
    // Allocate and initialize arrays.
    //================================================================
    double *A =
        (double*)malloc((size_t)lda*m*sizeof(double));
    assert(A != NULL);

    double *B =
        (double*)malloc((size_t)ldb*n*sizeof(double));
    assert(B != NULL);

    int *ipiv =NULL;
    ipiv = (int*)malloc((size_t)Am*sizeof(int));
    assert(ipiv != NULL);

    int seed[] = {0, 0, 0, 1};
    lapack_int retval;
    //=================================================================
    // Initialize the matrices.
    // Factor A into LU to get well-conditioned triangular matrices.
    // Use L for unit triangle, and U for non-unit triangle,
    // transposing as necessary.
    // (There is some danger, as L^T or U^T may be much worse conditioned
    // than L or U, but in practice it seems okay.
    // See Higham, Accuracy and Stability of Numerical Algorithms, ch 8.)
    //=================================================================
    retval = LAPACKE_dlarnv(1, seed, (size_t)lda*n, A);
    assert(retval == 0);

    for (int i=0; i< Am; i++) { // dgetrf is bugging with atlas
        A[lda*i + i] += i+1;
    }
    retval = LAPACKE_dlarnv(1, seed, (size_t)ldb*n, B);
    assert(retval == 0);

    double *Bref = NULL;
    if (test) {
        Bref = (double*)malloc(
            (size_t)ldb*n*sizeof(double));
        assert(Bref != NULL);

        memcpy(Bref, B, (size_t)ldb*n*sizeof(double));
    }

    //================================================================
    // Run and time.
    //================================================================
    double start = omp_get_wtime();
    NagError fail;
    #if defined(NAG)
    INIT_FAIL(fail);
    nag_dtrsm(
        CblasColMajor,
        side, uplo,
        transa, diag,
        m, n,
        alpha, A, lda,
        B, ldb, &fail);
    #else
    cblas_dtrsm(
        CblasColMajor,
        side, uplo,
        transa, diag,
        m, n,
        alpha, A, lda,
        B, ldb);
    #endif
    double stop = omp_get_wtime();
    double time = stop-start;
    param[PARAM_TIME].d = time;
    param[PARAM_GFLOPS].d = flops_dtrsm(side, m, n) / time / 1e9;

    //================================================================
    // Test results by checking the residual
    // ||alpha*B - A*X|| / (||A||*||X||)
    //================================================================
    if (test) {
        double zone  =  1.0;
        double zmone = -1.0;
        double work[1];

        // LAPACKE_[ds]lantr_work has a bug (returns 0)
        // in MKL <= 11.3.3 (at least). Fixed in LAPACK 3.6.1.
        // For now, call LAPACK directly.
        // LAPACK_dlantr is a macro for correct name mangling (e.g.
        // adding _ at the end) of the Fortran symbol.
        // The macro is either defined in lapacke.h, or in the file
        // core_lapack_d.h for the use with MKL.
        char normc = 'F';
        double Anorm = LAPACKE_dlantr_work(
            LAPACK_COL_MAJOR, 'F', uploc,
            diagc, Am, Am, A, lda, work);

        double Xnorm = LAPACKE_dlange_work(
                           LAPACK_COL_MAJOR, 'F', m, n, B, ldb, work);

        // B = A*X
        INIT_FAIL(fail);
        nag_dtrmm(
            CblasColMajor,
            (CBLAS_SIDE)side, (CBLAS_UPLO)uplo,
            (CBLAS_TRANSPOSE)transa, (CBLAS_DIAG)diag,
            m, n,
            (zone), A, lda,
            B, ldb, &fail);

        // B -= alpha*Bref
        cblas_dscal((size_t)ldb*n, (alpha), Bref, 1);
        cblas_daxpy((size_t)ldb*n, (zmone), Bref, 1, B, 1);

        double error = LAPACKE_dlange_work(
                           LAPACK_COL_MAJOR, 'F', m, n, B, ldb, work);
        if (Anorm * Xnorm != 0)
            error /= (Anorm * Xnorm);

        param[PARAM_ERROR].d = error;
        param[PARAM_SUCCESS].i = error < tol;
    }

    //================================================================
    // Free arrays.
    //================================================================
    free(A);
    free(B);
    if (test) {
        free(ipiv);
        free(Bref);
    }
}
