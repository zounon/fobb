#include "test.h"
#include "flops.h"
#include <math.h>
#include <string.h>

//============================================
//    Include files for orrectness checking
//============================================
#if !defined(MKL)
    #include <cblas.h>
    #include <lapacke.h>
#endif
#include <nag.h>
#include <nagf16.h>
#include <nagf07.h>


#if defined(ATLAS) ||  defined(BLIS)
    // Intel mkl_cblas.h and Netlib do: typedef enum {...} CBLAS_ORDER;
    // OpenBLAS  cblas.h does: typedef enum CBLAS_ORDER {...} CBLAS_ORDER;
    // BLIS cblas.h and ATLAS do enum CBLAS_ORDER {...}
    // We use (CBLAS_ORDER), so add these typedefs for BLIS and ATLAS.
    #ifndef OPENBLAS_VERSION
        typedef enum CBLAS_ORDER CBLAS_ORDER;
        typedef enum CBLAS_TRANSPOSE CBLAS_TRANSPOSE;
        typedef enum CBLAS_UPLO CBLAS_UPLO;
        typedef enum CBLAS_DIAG CBLAS_DIAG;
        typedef enum CBLAS_SIDE CBLAS_SIDE;
   #endif
#elif defined(MKL)
    #include <mkl.h>
#endif

#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <omp.h>


/***************************************************************************//**
 *
 * @brief Tests DGEMM.
 *
 * @param[in]  param - array of parameters
 * @param[out] info  - string of column labels or column values; length InfoLen
 *
 * If param is NULL and info is NULL,     print usage and return.
 * If param is NULL and info is non-NULL, set info to column labels and return.
 * If param is non-NULL and info is non-NULL, set info to column values
 * and run test.
 ******************************************************************************/
void test_dgemm(param_value_t param[], char *info)
{

    //================================================================
    // Print usage info or return column labels or values.
    //================================================================
    if (param == NULL) {
        if (info == NULL) {
            // Print usage info.
            print_usage(PARAM_TRANSA);
            print_usage(PARAM_TRANSB);
            print_usage(PARAM_M);
            print_usage(PARAM_N);
            print_usage(PARAM_K);
            print_usage(PARAM_ALPHA);
            print_usage(PARAM_BETA);
        }
        else {
            // Return column labels.
            snprintf(info, InfoLen,
                     "%*s %*s %*s %*s %*s %*s %*s",
                     InfoSpacing, "TransA",
                     InfoSpacing, "TransB",
                     InfoSpacing, "M",
                     InfoSpacing, "N",
                     InfoSpacing, "K",
                     InfoSpacing, "Alpha",
                     InfoSpacing, "Beta");

        }
        return;
    }
    // Return column values.
    snprintf(info, InfoLen,
             "%*c %*c %*d %*d %*d %*f %*f",
             InfoSpacing, param[PARAM_TRANSA].c,
             InfoSpacing, param[PARAM_TRANSB].c,
             InfoSpacing, param[PARAM_M].i,
             InfoSpacing, param[PARAM_N].i,
             InfoSpacing, param[PARAM_K].i,
             InfoSpacing, creal(param[PARAM_ALPHA].z),
             InfoSpacing, creal(param[PARAM_BETA].z));

    //================================================================
    // Set parameters.
    //================================================================
    char transa_c = param[PARAM_TRANSA].c;
    char transb_c = param[PARAM_TRANSB].c;

    int m = param[PARAM_M].i;
    int n = param[PARAM_N].i;
    int k = param[PARAM_K].i;

    int Am, An;
    int Bm, Bn;
    int Cm, Cn;

    CBLAS_TRANSPOSE transa, transb;

    if (transa_c == 'n') {
        transa = CblasNoTrans; 
        Am = m;
        An = k;
    }
    else {
        transa = CblasTrans; 
        Am = k;
        An = m;
    }
    if (transb_c == 'n') {
        transb = CblasNoTrans; 
        Bm = k;
        Bn = n;
    }
    else {
        transb = CblasTrans; 
        Bm = n;
        Bn = k;
    }
    Cm = m;
    Cn = n;

    int lda = imax(1, Am);
    int ldb = imax(1, Bm);
    int ldc = imax(1, Cm);

    double alpha = creal(param[PARAM_ALPHA].z);
    double beta  = creal(param[PARAM_BETA].z);

    int    test = param[PARAM_TEST].c == 'y';
    double tol  = param[PARAM_TOL].d * LAPACKE_dlamch('E');
    //================================================================
    // Allocate and initialize arrays
    //================================================================
    double *A = (double*)malloc((size_t)lda*An*sizeof(double));
    assert(A != NULL);

    double *B = (double*)malloc((size_t)ldb*Bn*sizeof(double));
    assert(B != NULL);

    double *C = (double*)malloc((size_t)ldc*Cn*sizeof(double));
    assert(C != NULL);

    int seed[] = {0, 0, 0, 1};
    lapack_int retval;
    retval = LAPACKE_dlarnv(1, seed, (size_t)lda*An, A);
    assert(retval == 0);

    retval = LAPACKE_dlarnv(1, seed, (size_t)ldb*Bn, B);
    assert(retval == 0);

    retval = LAPACKE_dlarnv(1, seed, (size_t)ldc*Cn, C);
    assert(retval == 0);

    double *Cref = NULL;
    if (test) {
        Cref = (double*)malloc((size_t)ldc*Cn*sizeof(double));
        assert(Cref != NULL);
        LAPACKE_dlacpy(LAPACK_COL_MAJOR, 'A', Cm, Cn, C, ldc, Cref, ldc);
    }

    //================================================================
    // Run and time 
    //================================================================
    NagError fail;
    INIT_FAIL(fail);
    double  start = omp_get_wtime();
    #if defined(NAG)
    nag_dgemm(
        CblasColMajor,
        transa, transb,
        m, n, k,
        alpha, A, lda,
        B, ldb,
        beta, C, ldc, &fail);

    if (fail.code != NE_NOERROR) {
        printf("Error from nag_dgemm .\n%s\n", fail.message);
        return;
    }
    #else
    cblas_dgemm(
        CblasColMajor,
        transa, transb,
        m, n, k,
        alpha, A, lda,
        B, ldb,
        beta, C, ldc);
    #endif
    double  stop = omp_get_wtime();
    double  time = stop-start;

    param[PARAM_TIME].d   = time;
    param[PARAM_GFLOPS].d = flops_dgemm(m,n,k) / time / 1e9;

    //================================================================
    // Test results by comparing to a reference implementation.
    //================================================================
    if (test) {
        // |R - R_ref|_p < gamma_{k+2} * |alpha| * |A|_p * |B|_p +
        //                 gamma_2 * |beta| * |C|_p
        // holds component-wise or with |.|_p as 1, inf, or Frobenius norm.
        // gamma_k = k*eps / (1 - k*eps), but we use
        // gamma_k = sqrt(k)*eps as a statistical average case.
        // Using 3*eps covers complex arithmetic.
        // See Higham, Accuracy and Stability of Numerical Algorithms, ch 2-3.
        double work[1];
        double Anorm = LAPACKE_dlange_work(
                           LAPACK_COL_MAJOR, 'F', Am, An, A,    lda, work);
        double Bnorm = LAPACKE_dlange_work(
                           LAPACK_COL_MAJOR, 'F', Bm, Bn, B,    ldb, work);
        double Cnorm = LAPACKE_dlange_work(
                           LAPACK_COL_MAJOR, 'F', Cm, Cn, Cref, ldc, work);

        INIT_FAIL(fail);
        nag_dgemm(
            CblasColMajor,
            transa, transb,
            m, n, k,
            alpha, A, lda,
            B, ldb,
            beta, Cref, ldc, &fail);
        if (fail.code != NE_NOERROR) {
            printf("Error from nag_dgemm .\n%s\n", fail.message);
            return;
        }

        double zmone = -1.0;
        cblas_daxpy((size_t)ldc*Cn, (zmone), Cref, 1, C, 1);

        double my_error = 0.0;
        for (int i = 0; i< Cn*Cn; i++) {
            my_error += fabs(C[i]);
        }
        double error = LAPACKE_dlange_work(
                           LAPACK_COL_MAJOR, 'F', Cm, Cn, C, ldc, work);
        double normalize = sqrt((double)k+2) * fabs(alpha) * Anorm * Bnorm
                         + 2 * fabs(beta) * Cnorm;
        if (normalize != 0)
            error /= normalize;

        param[PARAM_ERROR].d   = error;
        param[PARAM_SUCCESS].i = error < tol;
    }
    //================================================================
    // Free arrays.
    //================================================================
    free(A);
    free(B);
    free(C);
    if (test)
        free(Cref);
}
