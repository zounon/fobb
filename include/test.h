#ifndef TEST_NLALIBS_H
#define TEST_NLALIBS_H

#include <complex.h>

typedef float  _Complex fobb_complex32_t;
typedef double _Complex fobb_complex64_t;

//==============================================================================
// parameter labels
//==============================================================================
// @note Order of input parameters in typedef must coincide with
//       order of parameter descriptions in char array below
typedef enum {
    //------------------------------------------------------
    // input parameters
    //------------------------------------------------------
    PARAM_ITER,    // outer product iteration?
    PARAM_OUTER,   // outer product iteration?
    PARAM_TEST,    // test the solution?
    PARAM_TOL,     // tolerance
    PARAM_TRANS,   // transposition
    PARAM_TRANSA,  // transposition of A
    PARAM_TRANSB,  // transposition of B
    PARAM_SIDE,    // left of right side application
    PARAM_UPLO,    // general rectangular or upper or lower triangular
    PARAM_DIAG,    // non-unit or unit diagonal
    PARAM_COLROW,  // columnwise or rowwise operation
    PARAM_M,       // M dimension
    PARAM_N,       // N dimension
    PARAM_K,       // K dimension
    PARAM_NRHS,    // number of RHS
                   //   eigenvalues only (V) or eigenvalues and eigenvectors (W)
    PARAM_ALPHA,   // scalar alpha
    PARAM_BETA,    // scalar beta

    //------------------------------------------------------
    // output parameters
    //------------------------------------------------------
    PARAM_TIME,    // time to solution
    PARAM_GFLOPS,  // GFLOPS rate
    PARAM_SUCCESS, // success indicator
    PARAM_ERROR,   // numerical error
    PARAM_ORTHO,   // orthogonality error
    //------------------------------------------------------
    // Keep at the end!
    //------------------------------------------------------
    PARAM_SIZEOF   // size of parameter array
} param_label_t;

//==============================================================================
// parameter descriptions
//==============================================================================
// @note Order of parameter descriptions in char array must coincide with
//       order of input parameters in typedef above
static const char * const ParamUsage[][2] = {
    //------------------------------------------------------
    // input parameters
    //------------------------------------------------------
    {"--iter=", "number of iterations per set of parameters [default: 1]"},
    {"--outer=[y|n]", "outer product iteration [default: n]"},
    {"--test=[y|n]", "test the solution [default: y]"},
    {"--tol=", "tolerance [default: 50]"},
    {"--trans=[n|t|c]", "transposition [default: n]"},
    {"--transa=[n|t|c]", "transposition of A [default: n]"},
    {"--transb=[n|t|c]", "transposition of B [default: n]"},
    {"--side=[l|r]", "left of right side application [default: l]"},
    {"--uplo=[g|u|l]",
        "general rectangular or upper or lower triangular matrix [default: l]"},
    {"--diag=[n|u]", "not unit triangular or unit matrix [default: n]"},
    {"--colrow=[c|r]", "columnwise or rowwise [default: c]"},
    {"--m=", "M dimension (number of rows) [default: 1000]"},
    {"--n=", "N dimension (number of rows or columns) [default: 1000]"},
    {"--k=", "K dimension (number of rows or columns) [default: 1000]"},
    {"--nrhs=", "NHRS dimension (number of columns) [default: 1000]"},
    {"--alpha=", "scalar alpha"},
    {"--beta=", "scalar beta"},


    //------------------------------------------------------
    // output parameters
    //------------------------------------------------------
    // these are not used, except to assert sizeof(ParamUsage) == PARAM_SIZEOF
    {"success", "success indicator"},
    {"error",   "numerical error"},
    {"ortho",   "orthogonality error"},
    {"time",    "time to solution"},
    {"gflops",  "GFLOPS rate"},
};

//==============================================================================
// tester infrastructure
//==============================================================================
// parameter value type
typedef union {
    int i;                 // integer
    char c;                // character
    double d;              // double precision
    fobb_complex64_t z;  // double complex
} param_value_t;

// parameter type
typedef struct {
    int num;            // number of values for a parameter
    int pos;            // current position in the array
    int size;           // size of parameter values array
    param_value_t *val; // array of values for a parameter
} param_t;


// initial size of values array
static const int InitValArraySize = 1024;

// indentation of option descriptions
static const int DescriptionIndent = -20;

// maximum length of info string
static const int InfoLen = 1024;

// spacing in info output string
// each column is InfoSpacing wide + 1 space between columns
static const int InfoSpacing = 11;

// function declarations
void print_main_usage();
void print_routine_usage(const char *name);
void print_usage(int label);
int  test_routine(int test, const char *name, param_value_t param[]);
void run_routine(const char *name, param_value_t pval[], char *info);
void param_init(param_t param[]);
int  param_read(int argc, char **argv, param_t param[]);
int  param_starts_with(const char *str, const char *prefix);
int  param_scan_int(const char *str, param_t *param);
int  param_scan_char(const char *str, param_t *param);
int  param_scan_double(const char *str, param_t *param);
int  param_scan_complex(const char *str, param_t *param);
void param_add_int(int val, param_t *param);
void param_add_char(char cval, param_t *param);
void param_add_double(double dval, param_t *param);
void param_add_complex(fobb_complex64_t zval, param_t *param);
int  param_step_inner(param_t param[]);
int  param_step_outer(param_t param[], int idx);
int  param_snap(param_t param[], param_value_t value[]);

//==============================================================================
static inline int imin(int a, int b)
{
    if (a < b)
        return a;
    else
        return b;
}

//==============================================================================
static inline int imax(int a, int b)
{
    if (a > b)
        return a;
    else
        return b;
}

#include "test_d.h"
#include "test_s.h"

#endif // TEST_H
