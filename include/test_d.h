#ifndef TEST_D_H
#define TEST_D_H

//==============================================================================
// test routines
//==============================================================================
void test_dgemm(param_value_t param[], char *info);
void test_dtrsm(param_value_t param[], char *info);
void test_dtrmm(param_value_t param[], char *info);
void test_dsymm(param_value_t param[], char *info);
#endif // TEST_D_H
