#ifndef TEST_S_H
#define TEST_S_H

//==============================================================================
// test routines
//==============================================================================
void test_sgemm(param_value_t param[], char *info);
void test_strsm(param_value_t param[], char *info);
void test_strmm(param_value_t param[], char *info);
void test_ssymm(param_value_t param[], char *info);
#endif // TEST_S_H
